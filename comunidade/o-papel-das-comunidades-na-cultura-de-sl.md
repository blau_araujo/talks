# O PAPEL DAS COMUNIDADES NA CULTURA DE SOFTWARE LIVRE

## Michelangelo

Ao ser perguntado como teria feito "Davi", uma escultura
de quase 4,5m em um único bloco de mármore, Michelangelo
respondeu:

"Fiquei um bom tempo olhando o mármore até enxergar Davi;
depois peguei o martelo e o cinzel e retirei da pedra
tudo que não era Davi"

Nesta apresentação, nós vamos dar uma boa olhada no
pensamento petrificado do nosso tempo até conseguirmos
enxergar a cultura de Software Livre tal como ela é,
para tentarmos extrair daquilo que ela não é
a sua mais pura escultura: as comunidades.

## O que é Software Livre?

O Software Livre precisa ser entendido em pelo menos
quatro planos:

* Jurídico : um modelo de licenciamento
* Economico: um modelo de desenvolvimento
* Político : um movimento social
* Ético    : um sistema de valores

Retire-se qualquer um desses aspectos e já não estaremos
mais falando de Software Livre

### Modelo de Licenciamento

Para ser Livre, a licença deve grantir:

* A liberdade de executar o software para qualquer propósito

* A liberdade de estudar como o programa funciona e mudá-lo
  para que ele faça o que você desejar

* A liberdade de redistribuir e fazer cópias para que você
  possa ajudar o próximo

* A liberdade de melhorar o programa e distribuir seus
  melhoramentos para o público para que toda a comunidade
  seja beneficiada

### Modelo de Desenvolvimento

* Livre não é gratuito

* Ênfase no desenvolvimento colaborativo

* Ênfase na remuneração através do acréscimo de valor
  com suporte, treinamento, integração, customização,
  manutenção, etc

* Estímulo a contribuições voluntárias em dinheiro
  e financiamento coletivo

## Movimento Social

* Defesa das liberdades do consumidor de software

* Uso de Software Livre na administração pública

* Uso e ensino de Software Livre em escolas e universidades

* Defesa da adoção de formatos abertos de documentos

* Defesa da privacidade e da segurança de dados

* Geração de oportunidades de trabalho

* Inclusão digital

## Sistema de Valores

* Sistema de valores não é "filosofia"

* Mas, podemos dizer que é "uma filosofia", no sentido
  de princípios, de um modo de viver

* O Software Livre jamais seria uma realidade nem teria
  alcançado o sucesso que alcançou sem os seus valores

* Sem valores, não há motivação para colaborar, contribuir
  ou desenvolver nos modelos do Software Livre

* Sem valores, não há cultura

* Sem cultura, não há comunidade

## O que é Cultura?

Conjunto de ideias, comportamentos e práticas sociais

### Portanto...

O Software Livre é uma cultura!

## O que é Comunidade?

* Uma boa vida é aquela vivida em harmonia
  consigo mesmo, os outros e a natureza

* Nós só temos uma individualidade em função
  da coletividade

* A comunidade é o coletivo definindo o indivíduo

* Aristóteles: O verdadeiro ser do ser humano
  é ser para o outro

* Ubuntu: eu sou o que sou pelo que nós somos

### Toda comunidade surge da necessidade

* Ninguém é autossuficiente

* Ninguém tem todas as respostas

* Ninguém faz todas as perguntas

* Ninguém tem todos os recursos

* A sobrevivência depende de que cada um complete
  aquilo que falta no outro

### Toda comunidade exige um intercâmbio justo

* Sem comunidade, não há sobrevivência [*]

* Sem intercâmbio, não há comunidade [*]

* Sem inclusão, não há intercâmbio

* Comunidades pressupõem um propósito em comum

* Comunidades pressupõem benefício mútuo

* Comunidades pressupõem inclusão e acolhimento

[*] - https://sbgfilosofia.blogspot.com/2009/03/viver-em-comunidade.html

### No contexto do Software Livre

* Associação informal entre pessoas que compartilham os
  ideais, os comportamentos e as práticas da cultura
  de Software Livre

* Isso inclui desenvolvedores e usuários de Software Live

* Mas não basta usar ou desenvolver Software Livre para
  que se configure uma comunidade de Software Livre

### O que não é uma comunidade de Software Livre

* Comunidades de Software Livre não são sobre "tecnologia"

* Comunidades de Software Livre não são sobre "funcionar"

* Comunidades de Software Livre não são sobre "usar Linux"

* Comunidades de Software Livre não são sobre erradicar a
  existência, o uso ou os usuários de software proprietário

### Qual é o papel de uma comunidade de Software Livre?

* As comunidades de Software Livre existem para viabilizar
  e tornar sustentável um modelo de desenvolvimento justo,
  tanto para desenvolvedores quanto para os usuários
  
### Sem as comunidades, a cultura de Software Livre morre

* Modelo de negócios altamente dependente de doações,
  financiamento coletivo, reconhecimento e valorização
  de princípios

* Modelo de desenvolvimento altamente dependente de
  cooperação e do retorno dos usuários e de outros
  desenvolvedores

* Processo de adesão pública altamente dependente
  da representação organizada dos interesses da
  sociedade

* Conscientização do consumidor e do desenvolvedor
  de software altamente dependente da exposição
  constante dos benefícios para ambas as partes

### Sem um intercâmbio justo, não há comunidades

* Licenças só podem garantir a parte da justiça
  do intercâmbio no que tange ao uso do software

* É preciso ver as necessidades do desenvolvedor
  e de quem produz e publica conteúdo livre

* É preciso ver as necessidades de quem se dedica
  a prestar serviços em torno do Software Livre

* Em vez de cobrar "features", precisamos aprender
  a colaborar para que elas sejam implementadas,
  nem que seja em trabalhos derivados

* Doações e outras contribuições financeiras, se
  vistas individualmente, podem sair mais caras
  do que pagar por licenças proprietárias, mas
  isso pode ser muito diluído com o envolvimento
  da comunidade

### Sem inclusão, não há intercâmbio justo

* Em comunidade, cada membro conta

* Desenvolvedores não podem se excluir das
  comunidades surgidas em torno de seu trabalho

* Usuários não podem deixar que os desenvolvedores
  se coloquem numa posição separada da comunidade

* Empresas e organizações precisam ser inseridas
  nas comunidades e dialogar com os interesses de
  toda a coletividade

* Na cultura de Software Livre, a relação nunca é
  apenas com o programa que está sendo executado

* Não basta fornecer uma boa tecnologia

* Não basta abrir o código

* Não basta ser de graça

* Não basta usar Linux

* Se todas as partes não se enxergarem como
  participantes ativos e importantes da cultura,
  não há uma comunidade de  Software Livre.

## Obrigado!
  
  
